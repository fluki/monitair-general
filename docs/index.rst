Monit'Air : La solution de de suivi de la qualité de l'air !
============================================================

Monit'Air Server
----------------

Monit'Air Server est le coeur de la solution de suivi. C'est cette partie qui stocke et gère la réception des mesures 
de la qualité de l'air.

.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Monit'Air Server

  /monitair-server/getting-started
  /monitair-server/database
  /monitair-server/using-the-api

Monit'Air Client
----------------


.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Monit'Air Client

Infrastructure
--------------

Pour le développement de la solution, une infrastructure virutalisé a été mise en place pour chacun des membres de l'équipe projet.

.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Infrastructure

  /infra/overview
