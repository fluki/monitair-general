Getting started
================

Prérequis
---------

Le serveur Monit'air est une application Python 3. Le stockage des donénes se fait au travers d'une base de données SQL. L'installation des dépendances sur une Debiant se fait par la commande suivante :

.. code-block:: bash
  
  apt install python3 python3-pip mariadb-server libmariadb-dev-compat

Installation
------------

Commencez par cloner le dépôt git :

.. code-block:: bash

  git clone https://gitlab.ensimag.fr/monit-air/monit-air-server.git

Exécutez le script d'installation

.. code-block:: bash
  
  cd monit-air-server
  bash install.sh

.. warning::

  Il est fortement recommandé, de laisser le script installer apache2, sinon vous devrez configurer vous même le serveur web en suivant la documentation Django : https://docs.djangoproject.com/fr/3.2/howto/deployment/

Une fois le processus d'installation terminé, vous devez créer la base de données. D'abord connectez vous à l'instance SQL

.. code-block:: bash

    mysql -u root

Puis créez la base de données et l'utilisateur qui aura les droits dessus

.. code-block:: sql

  create database monitair;
  grant all privileges on monitair.* TO 'monitair_backend'@'localhost' identified by 'password';
  flush privileges;

Configuration
-------------

La configuration du serveur Mos le fichier */etc/monitair-server/config.toml*.
Il est important de configuration les informations de connexion à la base de données en adaptant l'exemple ci-dessous :

.. code-block:: toml

  [database]
  DATABASE_NAME = 'monitair'
  DATABSE_USER = 'monitair_backend'
  DATABSE_PASSWORD='password'

La création du schéma de la base se fait à l'aide des utilitaires django. Pour les utiliser, il faut activer l'environnement virtuel python

.. code-block:: bash

  source /etc/monitair-server/virtualenv/monitair-server

Ensuite lancer la création du schéma

.. code-block:: bash
  
  cd /var/www/monitair_server
  python3 manage.py migrate