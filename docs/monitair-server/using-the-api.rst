Utilisation de l'API
====================

Location
--------

Liste des emplacements
++++++++++++++++++++++

.. http:get:: /api/location/

    Récupération de la liste de toutes les cartes

    **Exemple de requêtes**

    .. tabs::

        .. code-tab:: bash

            $ curl https://monitair.giordana.lan/api/location

        .. code-tab:: python

            import requests

            url = 'https://monitair.giordana.lan/api/location'
            req = requests.get(url)

            print(req.status_code)
            print(req.headers)
            print(req.text)
        
        .. code-tab:: javascript

            const headers = new Headers();
            headers.append('content-type', 'application/json');

            const init = {
            method: 'GET',
            headers
            };

            fetch('https://monitair.giordana.lan/api/location', init)
            .then((response) => {
            return response.json(); // or .text() or .blob() ...
            })
            .then((text) => {
            // text is the response body
            })
            .catch((e) => {
            // error in e.message
            });
            

    **Exemple de réponse**

    .. sourcecode:: json

        [
            {
                "id": 1,
                "name": "A001"
            }
        ]

Création d'un emplacement
+++++++++++++++++++++++++

.. http:post:: /api/location

    Création d'une location

    **Exemple de requêtes**

    .. tabs::

        .. code-tab:: bash

            $ curl "https://monitair.giordana.lan/api/location" \
            -X POST \
            -d "{\n  \"name\": \"A001\"\n}" \
            -H "content-type: application/json" 

        .. code-tab:: python

            import requests

            url = 'https://monitair.giordana.lan/api/location'
            headers = {'content-type': 'application/json'}
            body = """{
            "name": "A001"
            }"""

            req = requests.post(url, headers=headers, data=body)

            print(req.status_code)
            print(req.headers)
            print(req.text)
            

    **Exemple de réponse**

    .. sourcecode:: json

        {
            "id": 1,
            "name": "A001"
        }

Sensor
------

Liste des capteurs
++++++++++++++++++

.. http:get:: /api/sensor/

    Récupération de la liste de toutes les capteurs

    **Exemple de requêtes**

    .. tabs::

        .. code-tab:: bash

            $ curl https://monitair.giordana.lan/api/sensor

        .. code-tab:: python

            import requests

            url = 'https://monitair.giordana.lan/api/sensor'
            req = requests.get(url)

            print(req.status_code)
            print(req.headers)
            print(req.text)
        
        .. code-tab:: javascript

            const headers = new Headers();
            headers.append('content-type', 'application/json');

            const init = {
            method: 'GET',
            headers
            };

            fetch('https://monitair.giordana.lan/api/sensor', init)
            .then((response) => {
            return response.json(); // or .text() or .blob() ...
            })
            .then((text) => {
            // text is the response body
            })
            .catch((e) => {
            // error in e.message
            });
            
            

    **Exemple de réponse**

    .. sourcecode:: json

        [
            {
                "id": 1,
                "name": "Sensor I2C",
                "last_calibration": null,
                "location": 1
            },
            {
                "id": 2,
                "name": "Test Sensor",
                "last_calibration": null,
                "location": 1
            }
        ]

Création d'un capteur
+++++++++++++++++++++

.. http:post:: /api/sensor/

    Création d'une carte

    **Exemple de requêtes**

    .. tabs::

        .. code-tab:: bash

            $ curl "https://monitair.giordana.lan/api/sensor" \
            -X POST \
            -d "{\n    \"name\": \"Test Sensor\",\n    \"location\": 1\n}" \
            -H "content-type: application/json"  

        .. code-tab:: python

            import requests

            url = 'https://monitair.giordana.lan/api/sensor'
            headers = {'content-type': 'application/json'}
            body = """{
                "name": "Test Sensor",
                "location": 1
            }"""

            req = requests.post(url, headers=headers, data=body)

            print(req.status_code)
            print(req.headers)
            print(req.text)
            

    **Exemple de réponse**

    .. sourcecode:: json

        {
            "id": 2,
            "name": "Test Sensor",
            "last_calibration": null,
            "location": 1
        }

Measurement
-----------

Liste des mesures
+++++++++++++++++

.. http:get:: /api/measurement/

    Récupération de la liste de toutes les mesures

    **Exemple de requêtes**

    .. tabs::

        .. code-tab:: bash

            $ curl https://monitair.giordana.lan/api/measurement

        .. code-tab:: python

            import requests

            url = 'https://monitair.giordana.lan/api/measurement'
            req = requests.get(url)

            print(req.status_code)
            print(req.headers)
            print(req.text)
        
        .. code-tab:: javascript

            const headers = new Headers();
            headers.append('content-type', 'application/json');

            const init = {
            method: 'GET',
            headers
            };

            fetch('https://monitair.giordana.lan/api/measurement/', init)
            .then((response) => {
            return response.json(); // or .text() or .blob() ...
            })
            .then((text) => {
            // text is the response body
            })
            .catch((e) => {
            // error in e.message
            });

            

    **Exemple de réponse**

    .. sourcecode:: json

        [
            {
                "id": 1,
                "value": 578.0,
                "timestamp": "2021-09-05T22:05:49.191589+02:00",
                "sensor": 1,
                "location": 1
            },
            {
                "id": 2,
                "value": 578.0,
                "timestamp": "2021-09-05T22:05:50.258172+02:00",
                "sensor": 1,
                "location": 1
            },
        ]

Création d'une mesure
+++++++++++++++++++++

.. http:post:: /api/measurement/

    Création d'une mesure

    **Exemple de requêtes**

    .. tabs::

        .. code-tab:: bash

            $ curl "https://monitair.giordana.lan/api/measurement" \
            -X POST \
            -d "{\n    \"value\": 553,\n    \"sensor\": 1,\n    \"location\": 1\n}" \
            -H "content-type: application/json" 

        .. code-tab:: python

            import requests

            url = 'https://monitair.giordana.lan/api/measurement'
            headers = {'content-type': 'application/json'}
            body = """{
                "value": 553,
                "sensor": 1,
                "location": 1
            }"""

            req = requests.post(url, headers=headers, data=body)

            print(req.status_code)
            print(req.headers)
            print(req.text)
            

    **Exemple de réponse**

    .. sourcecode:: json

        {
            "id": 590,
            "value": 553,
            "timestamp": "2021-10-08T11:06:16.449523+02:00",
            "sensor": 1,
            "location": 1
        }