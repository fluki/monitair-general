Base de données
===============

Schéma relationnel
------------------

Le schéma relationnel de la base de donnée est le suivant :

.. figure:: /_static/img/database/scheme.jpg
    :width: 75%
    :align: center

    Schéma relationnel de la base de données