Overview
========

Présentation
------------

L'infrastructure actuelle met à disposition les serveurs suivants :

.. figure:: /_static/img/infra/infra.jpg
    :width: 75%
    :align: center

    Schéma de l'infrastructure


Accès à l'infrastructure
------------------------

L'accès à l'infrastructure se fait au travers d'un VPN dédié. L'authentification se fait
à l'aide d'un couple certificat + radius.  
Le serveur radius assure l'authentification user/password sur l'ensemble de l'infrastrucutre

Pour tout problème, contactez Danaël Giordana : danael@giordana.cc

Accès aux machines
------------------

Une fois le VPN connecté, les machines sont accessibles en SSH avec une authentification par clef publique.
Pour se connecter, l'usage de l'adresse IP, du nom de la machine ou de l'alias sont possibles.
L'alias est la méthode recommandé.

.. note::
    En cas d'utilisation du nom ou de l'alias, il faut veiller à ajouter le suffixe DNS :
    giordana.lan

Pour demander l'ajout d'une clef publique ou pour tout problème, contactez Danaël Giordana : danel@giordana.cc

Usage des droits privilégiés
----------------------------

L'accès au droits privilégié root se fait par la commande ``sudo``