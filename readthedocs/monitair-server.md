# Monit'Air Server
## Introduction

## Prérequis

1. Installation des dépendances
Le serveur Monit'air est une application Python 3. Le stockage des donénes se fait au travers d'une base de données SQL. L'installation des dépendances sur une Debiant 10 se fait par la commande suivante :
```
apt install python3 python3-pip mariadb-server libmariadb-dev-compat
```

## Installation

1. Cloner le repo
```
git clone https://gitlab.ensimag.fr/monit-air/monit-air-server.git
```

2. Exécution du script d'installation (Debian only)
Nous avons développé un script qui automatise la plupart des tâches d'installation. Exécutez le et suivez les instructions.
Il est fortement recommandé, de laisser le script installer apache2, sinon vous devrez configurer vous même le serveur web en suivant la documentation Django : https://docs.djangoproject.com/fr/3.2/howto/deployment/

```
cd monit-air-server
bash install.sh
```

3. Configuratio de la base de données
Pour l'instant la configuration de la base de donnée n'est pas automatisé, vous devez donc le faire à la main
    1. Création de la base de données
    ```
    mysql -u root
    create database monitair;
    ```
    2. Création de l'utilisateur backend qui aura accès à la base de données
    ```
    grant all privileges on monitair.* TO 'monitair_backend'@'localhost' identified by 'password';
    flush privileges;
    ```

4. Configuration du serveur Monit'air
Après avoir créé une base de données et définit un utilisateur ayant les droits dessus. Vous devez les renseignez dans le fichier */etc/monitair-server/config.toml*. En reprenant notre exemple, voici ce que nous devrions configurer :
```toml
[database]
DATABASE_NAME = 'monitair'
DATABSE_USER = 'monitair_backend'
DATABSE_PASSWORD='password'
``` 

5. Création du schéma de la base de données
  1. Activation de l'envrionnement virtuelle 
  ```
  source /etc/monitair-server/virtualenv/monitair-server
  ```
  
  2. Création du schéma de la base
  ```
  cd /var/www/monitair_server
  python3 manage.py migrate
  ```